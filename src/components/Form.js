import React, { useState, useMemo } from "react";

import { Rend } from "./Rend";
import { getConfig } from "./config";

export const Form = () => {
    const [state, setState] = useState({
        fullName: "",
        email: "",
        phoneNumber: ""
    });

    const handleSubmit = e => {
        e.preventDefault();
        console.log("state", state);
    }

    const handleOnChange = field => event => {
        const { value } = event.target;
        setState(prevState => ({  ...prevState,  [field]: value  }));
    }

    const config = useMemo(() => {
        return getConfig({ state,  onChange: handleOnChange  });
    }, [state]);

    return (
        <form onSubmit={handleSubmit}>
            <Rend config={config} />
            <button>Submit</button>
        </form>
    );
}
import InputField from './InputField.js'

export const getConfig = ({ state, onChange }) => {
    return [
        {
            name: "FName",
            id: "FName",
            type: "text",
            label: "Full Name",
            component: InputField,
            value: state["FName"],
            onChange: onChange("FName"),
        },
        {
            name: "email",
            id: "email",
            type: "text",
            component: InputField,
            label: "Email",
            value: state["email"],
            onChange: onChange("email"),
        },
        {
            name: "phoneNumber",
            id: "email",
            type: "tel",
            component: InputField,
            label: "Phone Number",
            value: state["phoneNumber"],
            onChange: onChange("phoneNumber"),
        },
    ]
}
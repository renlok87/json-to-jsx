import React, { Fragment } from "react";

const mapPropsToConfig = config => {
    const configWithProps = [];

    config.forEach(item => {
        if (item.component) {
            const { component, ...props } = item;
            configWithProps.push({
                ...props,
                Component: component
            });
        }
    });

    return configWithProps;
};

export const Rend = ({ config }) => {
    if (!config) {
        throw new Error('если пришел лажовый json');
    }

    const configWithProps = mapPropsToConfig(config);

    const renderComponents = (items) => {
        return items.map(item => {
            const { Component, ...props } = item;
            return (
                <Fragment key={props.name}>
                    <Component {...props} />
                </Fragment>
            );
        })
    }

    return renderComponents(configWithProps)
};